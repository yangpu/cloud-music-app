import http from '@/utils/http'

export default {
  // 获取banner图
  Banner () {
    return http.get('banner')
  },

  // 推荐歌单
  Personalized () {
    return http.get('personalized')
  },

  // 新歌速送
  Newsong () {
    return http.get('personalized/newsong')
  }
}